var widget_defs = {
	"tag": "div",
	"classes": ["test-container"],
	"children": [
		{
			"name": "title",
			"tag": "h1",
			"text": "Mirror DOM Speed Test!"
		},
		{
			"name": "test_start_button",
			"tag": "button",
			"text": "Create 10,000 cells",
			"events": [
				["click", run_create_test]
			]
		},
		{
			"name": "test_update_button",
			"tag": "button",
			"text": "Update all cells",
			"events": [
				["click", run_update_test]
			]
		},
		{
			"name": "test_random_update_button",
			"tag": "button",
			"text": "500 random updates",
			"events": [
				["click", random_update_test]
			]
		},
		{
			"name": "clear_table",
			"tag": "button",
			"text": "Clear the Table",
			"events": [
				["click", clear_table]
			]
		},
		{
			"name": "test_table",
			"tag": "table",
			"classes": ["test-table"],
			"children": [
				{
					"name": "table_body",
					"tag": "tbody",
					"item_template": {
						"tag": "tr",
						"children": [
							{
								"name": "cell_1",
								"tag": "td",
								"variable": "cell_1"
							}
							,
							{
								"name": "cell_2",
								"tag": "td",
								"variable": "cell_2"
							}
							,
							{
								"name": "cell_3",
								"tag": "td",
								"variable": "cell_3"
							}
							,
							{
								"name": "cell_4",
								"tag": "td",
								"variable": "cell_4"
							}
							,
							{
								"name": "cell_5",
								"tag": "td",
								"variable": "cell_5"
							}
							,
							{
								"name": "cell_6",
								"tag": "td",
								"variable": "cell_6"
							}
							,
							{
								"name": "cell_7",
								"tag": "td",
								"variable": "cell_7"
							}
							,
							{
								"name": "cell_8",
								"tag": "td",
								"variable": "cell_8"
							},
							{
								"name": "cell_9",
								"tag": "td",
								"variable": "cell_9"
							}
							,
							{
								"name": "cell_10",
								"tag": "td",
								"variable": "cell_10"
							}
						]
					}
				}
			]
		}
	]
};