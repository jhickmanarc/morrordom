function clear_table() {
	var start_destroy = window.performance.now();
	var items_count = md.children.test_table.children.table_body.getItemCount();
	for (var i = 0; i < items_count; i++) {
		md.children.test_table.children.table_body.removeItem(0);
	}
	var end_destroy = window.performance.now();
	var duration = end_destroy - start_destroy;
	console.log("Removed " + items_count +" table rows in: " + duration + "ms");
}

function run_create_test() {
	// Populate 10,000 random numbers into the table
	var base = Math.round(Math.random() * 100);
	var start_create = window.performance.now();
	for (var i = 0; i < 1000; i++) {
		md.children.test_table.children.table_body.setData(-1, {
			"cell_1": base + i,
			"cell_2": base + i + 1,
			"cell_3": base + i + 2,
			"cell_4": base + i + 3,
			"cell_5": base + i + 4,
			"cell_6": base + i + 5,
			"cell_7": base + i + 6,
			"cell_8": base + i + 7,
			"cell_9": base + i + 8,
			"cell_10": base + i + 9
		});
	}
	var end_create = window.performance.now();
	var duration = end_create - start_create;
	console.log("Created 10,000 table cells in: " + duration + "ms");
	
}

function run_update_test() {
	var base = Math.round(Math.random() * 100);
	var items_count = md.children.test_table.children.table_body.getItemCount();
	var start_random_updates = window.performance.now();
	for (var i = 0; i < items_count; i++) {
		md.children.test_table.children.table_body.setData(i, {
			"cell_1": base + i,
			"cell_2": base + i + 1,
			"cell_3": base + i + 2,
			"cell_4": base + i + 3,
			"cell_5": base + i + 4,
			"cell_6": base + i + 5,
			"cell_7": base + i + 6,
			"cell_8": base + i + 7,
			"cell_9": base + i + 8,
			"cell_10": base + i + 9
		});
	}
	var end_random_updates = window.performance.now();
	var duration = end_random_updates - start_random_updates;
	console.log((items_count * 10) + " table updates in: " + duration + "ms");
}

function random_update_test() {
	var items_count = md.children.test_table.children.table_body.getItemCount();
	var newval = Math.round(Math.random() * 1000);
	var start_random_updates = window.performance.now();
	for (var i = 0; i < 100; i++) {
		var update_data = {};
		for (var j = 0; j < 5; j++) {
			var col = Math.round(Math.random() * 10);
			update_data["cell_" + col] = newval;
		}
		var row = Math.round(Math.random() * 1000);
		if (row <= items_count) {
			md.children.test_table.children.table_body.setData(row, update_data);
		}
	}
	var end_random_updates = window.performance.now();
	var duration = end_random_updates - start_random_updates;
	console.log("500 random updates in: " + duration + "ms");
}