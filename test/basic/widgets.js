function handle_button_clicked(e) {
	var form_data = md.children.test_form.getData();
	console.log("Text input value: " + form_data['text_value']);
	console.log("Selected option: " + form_data['select_widget']);
	console.log("Text Area contents: " + form_data['testing_textarea']);
	console.log("Check-box: " + form_data['checkbox_test']);
}

function table_cell_clicked(e) {
	var item_index = this.mirrorlink.getThisItem();
	alert("Table cell clicked. Row: " + item_index[0]);
}

var widget_defs = {
	"tag": "div",
	"classes": ["widget-container"],
	"children": [
		{
			"name": "heading",
			"tag": "h1",
			"props": {
				"id": "heading_1",
				"data-foo": "bar"
			},
			"classes": ["heading_class_1", "heading_class_2"],
			"variable": "header",
			"text": "Test title"
		},
		{
			"name": "column_containers",
			"canonical": "paragraphs",
			"tag": "div",
			"classes": ["columns_container"],
			"children": [
				{
					"name": "column_left",
					"tag": "p",
					"variable": "column_1",
					"text": "First Paragraph"
				},
				{
					"name": "column_right",
					"tag": "p",
					"variable": "column_2",
					"text": "Second paragraph"
				}
			]
		},
		{
			"name": "item_manager_test",
			"tag": "ul",
			"item_template": {
				"name": "link_item",
				"tag": "li",
				"children": [
					{
						"name": "link_list",
						"tag": "a",
						"variable": "link"
					}
				]
			}
		},
		{
			"name": "table_test",
			"tag": "table",
			"children": [
				{
					"name": "table_header_container",
					"tag": "thead",
					"children": [
						{
							"name": "table_header_row",
							"tag": "tr",
							"children": [
								{
									"name": "column_1",
									"tag": "th",
									"text": "Column 1",
									"variable": "left_column_header"
								},
								{
									"name": "column_2",
									"tag": "th",
									"text": "Column 2",
									"variable": "center_column_header"
								},
								{
									"name": "column_3",
									"tag": "th",
									"text": "Column 3",
									"variable": "right_column_header"
								}
							]
						}
					]
				},
				{
					"name": "table_body_container",
					"tag": "tbody",
					"item_template": {
						"tag": "tr",
						"children": [
							{
								"name": "column_1",
								"tag": "td",
								"variable": "left_cell",
								"events": [
									["click", table_cell_clicked]
								],
								"classes": ["clickable-row"]
							},
							{
								"name": "column_2",
								"tag": "td",
								"variable": "center_cell"
							},
							{
								"name": "column_3",
								"tag": "td",
								"variable": "right_cell"
							}
						]
					}
				}
			]
		},
		{
			"name": "test_form",
			"tag": "form",
			"children": [
				{
					"name": "text_input_test",
					"tag": "input",
					"props": {
						"type": "text",
						"name": "text_input_test"
					},
					"variable": "text_value"
				},
				{
					"name": "test_select",
					"tag": "select",
					"variable": "select_widget",
					"children": [
						{
							"name": "first_option",
							"tag": "option",
							"props": {
								"value": "1"
							},
							"text": "First Option"
						},
						{
							"name": "second_option",
							"tag": "option",
							"props": {
								"value": "2"
							},
							"text": "Second Option"
						},
						{
							"name": "third_option",
							"tag": "option",
							"props": {
								"value": "3"
							},
							"text": "Third Option"
						}
					]
				},
				{
					"name": "textarea_container",
					"tag": "div",
					"children": [
						{
							"name": "textarea_test",
							"tag": "textarea",
							"variable": "testing_textarea"
						}
					]
				},
				{
					"name": "checkbox_container",
					"tag": "div",
					"children": [
						{
							"name": "test_check",
							"tag": "input",
							"props": {
								"type": "checkbox",
								"id": "testing-check-box"
							},
							"variable": "checkbox_test"
						},
						{
							"name": "label_for_check",
							"tag": "label",
							"props": {
								"for": "testing-check-box"
							},
							"text": "Demonstration check-box."
						}
					]
				}
			]
		},
		{
			"name": "test_button",
			"tag": "button",
			"text": "Dump form element data.",
			"events": [
				["click", handle_button_clicked]
			]
		}
	]
};