function delete_row_clicked(e) {
	var item = this.mirrorlink.getThisItem();
	item[1].parentlink.removeItem(item[0]);
}

function add_row_clicked(e) {
	md.children.form_table.children.table_body.setData(-1, {});
}

function dump_forms_data() {
	var form_data = md.children.form_table.children.table_body.getItemsData();
	console.log(form_data);
}

var widget_defs = {
	"tag": "div",
	"classes": ["widget-container"],
	"children": [
		{
			"name": "form_table",
			"tag": "table",
			"children": [
				{
					"name": "table_headers",
					"tag": "thead",
					"children": [
						{
							"name": "header_row",
							"tag": "tr",
							"children": [
								{
									"name": "header_1",
									"tag": "th",
									"text": "Text input"
								},
								{
									"name": "header_2",
									"tag": "th",
									"text": "Select Control"
								},
								{
									"name": "header_3",
									"tag": "th",
									"text": "Check-Box"
								},
								{
									"name": "header_4",
									"tag": "th",
									"text": "Button"
								}
							]
						}
					]
				},
				{
					"name": "table_body",
					"tag": "tbody",
					"item_template": {
						"tag": "tr",
						"children": [
							{
								"name": "text_cell",
								"tag": "td",
								"children": [
									{
										"name": "text_cell_control",
										"tag": "input",
										"props": {
											"id": "text_cell_control",
											"type": "text"
										},
										"variable": "text"
									}
								]
							},
							{
								"name": "select_cell",
								"tag": "td",
								"children": [
									{
										"name": "select_control",
										"tag": "select",
										"variable": "select",
										"children": [
											{
												"name": "option_none",
												"tag": "option",
												"props": {
													"value": ""
												},
												"text": "Select ..."
											},
											{
												"name": "option_1",
												"tag": "option",
												"props": {
													"value": "1"
												},
												"text": "First Option"
											},
											{
												"name": "option_2",
												"tag": "option",
												"props": {
													"value": "2"
												},
												"text": "Second Option"
											},
											{
												"name": "option_3",
												"tag": "option",
												"props": {
													"value": "3"
												},
												"text": "Third Option"
											}
										]
									}
								]
							},
							{
								"name": "check_cell",
								"tag": "td",
								"children": [
									{
										"name": "checkbox_control",
										"tag": "input",
										"props": {
											"type": "checkbox",
											"id": "checkbox_control"
										},
										"variable": "checkbox"
									}
								]
							},
							{
								"name": "button_cell",
								"tag": "td",
								"children": [
									{
										"name": "button_control",
										"tag": "button",
										"text": "Delete Row",
										"events": [
											["click", delete_row_clicked]
										]
									}
								]
							}
						]
					}
				}
			]
		},
		{
			"name": "add_button",
			"tag": "button",
			"text": "Add Row",
			"events": [
				["click", add_row_clicked]
			]
		},
		{
			"name": "dump_data",
			"tag": "button",
			"text": "Get all data from the form Items",
			"events": [
				["click", dump_forms_data]
			]
		}
	]
};