// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

/*
 * Mirror DOM reactive DOM element creation and management with
 * efficient data patching, repeater management and clean deletion.
 *
 * 2018 - James Hickman <jameshickman.net>
 * Released as Open Source under LGPL2
 *
 * 2018-05-22:
 *   Added support for the "canonical" element tagging. Creates a dictionary
 *   of links to nodes as a shorter reference then the full object path.
 */
function MIRRORDOM () {
	function form_element_changed(e) {
		if (this.type == 'checkbox' || this.type == 'radio') {
			this.mirrorlink.varvalue = this.checked;
		}
		else {
			this.mirrorlink.varvalue = this.value;
		}
	}
	
	var cnames = {};
	if (arguments.length == 2) {
		this.canonical = {};
		cnames = this.canonical;
	}
	else {
		cnames = arguments[2];
	}
	var el_parent = arguments[0];
	var defs = arguments[1];
	
	
	this.tag = defs.tag;
	this.children = {};
	this.varname = false;
	this.item_template = false;
	this.el = document.createElement(defs.tag);
	this.el.mirrorlink = this;
	this.parentlink = el_parent.mirrorlink;
	if (defs.hasOwnProperty('props')) {
		for (k in defs.props) {
			if (defs.props.hasOwnProperty(k)) {
				this.el.setAttribute(k, defs.props[k]);
			}
		}
	}
	if (defs.hasOwnProperty('classes')) {
		for (var i = 0; i < defs.classes.length; i++) {
			this.el.classList.add(defs.classes[i]);
		}
	}
	if (defs.hasOwnProperty('variable')) {
		this.varname = defs.variable;
		this.varvalue = null;
		// Set up any input elements on-change event to
		// sync with the data value.
		if (defs.tag == 'input' || defs.tag == 'select' || defs.tag == 'textarea') {
			this.el.addEventListener('change', form_element_changed);
		}
	}
	if (defs.hasOwnProperty('children')) {
		for (var i = 0; i < defs.children.length; i++) {
			var nme = defs.children[i]['name'];
			this.children[nme] = new MIRRORDOM(this.el, defs.children[i], cnames);
		}
	}
	if (defs.hasOwnProperty('item_template')) {
		this.item_template = defs.item_template;
		this.items = [];
	}
	if (defs.hasOwnProperty('events')) {
		for (var i = 0; i < defs.events.length; i++) {
			this.el.addEventListener(defs.events[i][0], defs.events[i][1]);
		}
	}
	if (defs.hasOwnProperty('text')) {
		this.el.innerHTML = defs.text;
	}
	if (defs.hasOwnProperty('canonical')) {
		cnames[defs.canonical] = this;
	}
	el_parent.appendChild(this.el);
	return this;
}

MIRRORDOM.prototype.setData = function() {
	/*
	 * Add or update data within part of the Mirror DOM.
	 * Polymorphic function, if only an object of updates passed
	 * the current element or its children will be updated, patched
	 * from what is in the passed dictionary.
	 *
	 * The form with an integer index and then the update dictionary
	 * must target an item container. If the index is -1 append a new
	 * item, else target an existing for updating.
	 */
	function update_element(element, val) {
		switch(element.tag) {
			case 'a':
				if (val.hasOwnProperty('text')) {
					element.el.innerHTML = val.text;
				}
				if (val.hasOwnProperty('href')) {
					element.el.href = val.href;
				}
				if (val.hasOwnProperty('name')) {
					element.el.name = val.name;
				}
				break;
			case 'input':
				if (element.el.type == 'checkbox' || element.el.type == 'radio') {
					element.el.checked = val;
				}
				else {
					element.el.value = val;
				}
				break;
			case 'select':
				var opts = element.el.options;
				for (var opt, j = 0; opt = opts[j]; j++) {
					if (opt.value == val) {
						element.el.selectedIndex = j;
						break;
					}
				}
				break;
			default:
				// Default operation, set innerHTML
				element.el.innerHTML = val;
		}
	}
	function setvars(element, params) {
		if (element.varname !== false) {
			if (params.hasOwnProperty(element.varname)) {
				var newval = params[element.varname];
				if (element.varvalue != newval) {
					element.varvalue = replace_old(element.varvalue, newval);
					update_element(element, element.varvalue);
				}
			}
		}
		for (k in element.children) {
			if (element.children.hasOwnProperty(k)) {
				setvars(element.children[k], params);
			}
		}
	}
	function replace_old(oldval, newval) {
		if (typeof oldval == 'object' && oldval !== null) {
			for (k in newval) {
				oldval[k] = newval[k];
			}
			return oldval;
		}
		else {
			return newval;
		}
	}
	function uuidv4() {
	  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
	    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
	    return v.toString(16);
	  });
	}
	var params = {};
	var item_id = null;
	if (arguments.length == 1) {
		// Only parameter object
		params = arguments[0];
		setvars(this, params);
	}
	if (arguments.length == 2) {
		// Managed List Container.
		var idx = arguments[0];
		params = arguments[1];
		if (this.item_template !== false) {
			if (!this.hasOwnProperty('items')) {
				this.items = [];
			}
			if (idx == -1) {
				// Add new
				item_id = uuidv4();
				var line_item = new MIRRORDOM(this.el, this.item_template);
				line_item.item_id = item_id;
				line_item.el.setAttribute('data-item-id', item_id);
				setvars(line_item, params);
				this.items.push(line_item);
			}
			else if (idx < this.items.length) {
				setvars(this.items[idx], params);
				item_id = this.items[idx].el.getAttribute('data-item-id');
			}
		}
	}
	return item_id;
};

MIRRORDOM.prototype.getItemCount = function () {
	if (this.hasOwnProperty('items')) {
		return this.items.length;
	}
	return false;
};

MIRRORDOM.prototype.findItemByUUID = function (uuid) {
	/*
	 * Find current index of an item by UUID.
	 */
	if (this.hasOwnProperty('items')) {
		for (var i = 0; i < this.items.length; i++) {
			if (this.items[i].item_id == uuid) {
				return i;
			}
		}
	}
	return false;
};

MIRRORDOM.prototype.getThisItem = function () {
	/*
	 * Call from event handler on an Item,
	 * walks up the tree until finding the
	 * "data-item-id" attribute and returns it
	 * and pointer to the Item MIRRORDOM node.
	 * Else False if not an Item.
	 */
	var current_mirror = this;
	while (current_mirror.parentlink) {
		var item_id = current_mirror.el.getAttribute("data-item-id");
		if (item_id) {
			var idx = current_mirror.parentlink.items.indexOf(current_mirror);
			return [idx, current_mirror];
		}
		current_mirror = current_mirror.parentlink;
	}
	return false;
};

MIRRORDOM.prototype.removeItem = function (idx) {
	if (this.hasOwnProperty('items')) {
		this.items[idx].remove();
		this.items.remove(idx);
		return true;
	}
	return false;
};

MIRRORDOM.prototype.remove = function () {
	/*
	 * Cleanly delete and free a section of the DOM and Mirror DOM.
	 */
	function deleteSubbranch(branch) {
		// Loop over any children
		for (k in branch.children) {
			if (branch.children.hasOwnProperty(k)) {
				deleteSubbranch(branch.children[k]);
			}
		}
		// Loop over any Items
		if (branch.hasOwnProperty('items')) {
			for (var i = 0; i < branch.items.length; i++) {
				deleteSubbranch(branch.items[i]);
			}
		}
		branch.el.remove();
		delete branch;
	}
	deleteSubbranch(this);
};

MIRRORDOM.prototype.removeChildren = function () {
	for (k in this.children) {
		this.children[k].remove();
	}
};

MIRRORDOM.prototype.getData = function () {
	/*
	 * Walk current node and build an object of
	 * the variables from form elements.
	 */
	var d = {};
	function extract_data(branch) {
		for (k in branch.children) {
			if (branch.children.hasOwnProperty(k) && branch.children[k].tag != 'option') {
				extract_data(branch.children[k]);
			}
		}
		if (branch.varname != false && 
		    (branch.tag == 'input' || branch.tag == 'select' || branch.tag == 'textarea')
		   ) {
			d[branch.varname] = branch.varvalue;
		}
	}
	extract_data(this);
	return d;
}

MIRRORDOM.prototype.getItemsData = function () {
	/*
	 * Build an array of the variables in
	 * the items attached to this node.
	 */
	if (this.hasOwnProperty('items')) {
		var d = [];
		for (var i = 0; i < this.items.length; i++) {
			d.push({
				"item": this.items[i],
				"data": this.items[i].getData()
			});
		}
		return d;
	}
	return false;
}